import { createRouter, createWebHistory } from "vue-router";
import Home from "./components/page/HomePage.vue";

const routes = [
  { path: "/", component: Home },
  {
    path: "/private-project",
    component: () => import("./components/page/PrivateProject.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else if (to.hash) {
      return {
        el: to.hash,
        behavior: "smooth",
      };
    } else {
      return { top: 0 };
    }
  },
});

export default router;
