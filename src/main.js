import { createApp } from "vue";
import "./assets/main.css";
import "./assets/scss/main.scss";
// import "./assets/glitch.css";
import App from "./App.vue";
import router from "./router";
import "animate.css";

createApp(App).use(router).mount("#app");
